<?php
    function setupHeader($headerName){
        echo "<th>";
        // If not already ordering by this
        if($_GET['orderBy']!=$headerName){
            // Change orderBy to $headerName and sortOrder to default
            $linkQuery = $_SERVER['SCRIPT_NAME']."?";
            foreach ($_GET as $key => $value) {
                if($key=="orderBy"){
                    $linkQuery .= $key . "=" . $headerName . "&";
                } else if($key=="sortOrder"){
                    $linkQuery .= $key . "=" . "ASC" . "&";
                } else {
                    $linkQuery .= $key . "=" . $value . "&";
                }
            }
            echo "<a href='" . $linkQuery . "'>";
        } 
        // If already ordering by this
        else {
            // Change sortOrder to opposite of what it is
            $linkQuery = $_SERVER['SCRIPT_NAME']."?";
            foreach ($_GET as $key => $value) {
                if(!($key=="sortOrder")){
                    $linkQuery .= $key . "=" . $value . "&";
                } else {
                    if($value=="ASC"){
                        $linkQuery .= $key . "=" . "DESC" . "&";
                    } else {
                        $linkQuery .= $key . "=" . "ASC" . "&";
                    }
                }
            }
            echo "<a href='" . $linkQuery . "'>";
        }
        printHeaderName($headerName);

        // Pick one of the unicode arrows to denote ordering direction
        if($_GET['orderBy']!=$headerName){
            echo " ⇳";
        } else if($_GET['sortOrder']=="DESC"){
            echo " ⇧";
        } else {
            echo " ⇩";
        }

        echo "</a>";
        echo"</th>";
    }

    function printHeaderName($headerName){
        switch ($headerName) {
            case 'name':
                echo "Name";
                break;
            case 'time':
                echo "Last Seen";
                break;
            case 'position':
                echo "Position";
                break;
            case 'owner':
                echo "Owner";
                break;
            case 'type':
                echo "Type";
                break;
            default:
                break;
        }
    }
?>