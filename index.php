<?php include "headerSnippet.php" ?>
    <!-- First Hit Page Display -->
<?php if($_SERVER['REQUEST_METHOD']=='GET') { 
    ?>
    <!-- First panel, file upload section -->
    <div class="panelBackground">
    <h2>Contribute System Scan to Database</h2>
    <form method="post" action="upload.php" enctype="multipart/form-data">
        <label for="doc">System Scan as XML: </label><br />
        <fieldset>
        <input type="file" id="doc" name="doc"/>
        </fieldset><br />
        <input type="submit" name="upload" value="Upload File">
    </form>
</div>
    <p></p>
    <!-- Second panel, database searching section -->
    <div class="panelBackground">
    <h2>Entity Database Search</h2>
    <fieldset name="Data">
    <!-- Name Searching Section -->
    <legend>Entity Attributes</legend>
    <table class="centeredTable"><tr>
    <form method="get" action="search.php" enctype="multipart/form-data">
    <td><label for="entityName">Entity Name:</label></td>
    <td><input type="text" name="entityName" id="entityName" list="entityNames"></td>
    <td><input type="submit" value="Search by Name"><br /></td>
    </form>
    </tr>
        <datalist id="entityNames">
            <?php
               $query = "SELECT Name FROM Entities ORDER BY 1 ASC LIMIT 0, 100";
               $res = $mysqli->query($query);
               while ($row = $res->fetch_assoc()) {
                   echo "<option value = '{$row['Name']}'>'{$row['Name']}'</option>";
               }
               mysqli_free_result($res);
            ?>
        </datalist>
        <tr>
    <form method="get" action="search.php" enctype="multipart/form-data">
    <td><label for="ownerName">Owner Name:</label></td>
        <td><input type="text" name="ownerName" id="ownerName" list="ownerNames"></td>
    <td><input type="submit" value="Search by Owner"><br /></td>
    </form>
    </tr>
        <datalist id="ownerNames">
            <?php
               $query = "SELECT Name FROM Owners ORDER BY 1 ASC LIMIT 0, 100";
               $res = $mysqli->query($query);
               while ($row = $res->fetch_assoc()) {
                   echo "<option value = '{$row['Name']}'>'{$row['Name']}'</option>";
               }
               mysqli_free_result($res);
            ?>
        </datalist>
        <tr>
    <form method="get" action="search.php" enctype="multipart/form-data">
    <td><label for="entityType">Entity Type:</label></td>
        <td><input type="text" name="entityType" id="entityType" list="entityTypes"></td>
    <td><input type="submit" value="Search by Type"><br /></td>
    </form>
    </tr>
        <datalist id="entityTypes">
            <?php
               $query = "SELECT Name FROM Types ORDER BY 1 ASC LIMIT 0, 100";
               $res = $mysqli->query($query);
               while ($row = $res->fetch_assoc()) {
                   echo "<option value = '{$row['Name']}'>'{$row['Name']}'</option>";
               }
               mysqli_free_result($res);
            ?>
            </table>
        </datalist>
        </fieldset><br />
    <!-- Position Searching Section -->
    <form method="get" action="search.php" enctype="multipart/form-data">
        <fieldset name="Positions">
        <legend>Location</legend>
        <table class="centeredTable">
        <tr>
        <td><label for="galX">Galaxy X:</label></td>
        <td><input type="number" name="galX" id="galX"><br /></td>
        </tr>
        <tr>
        <td><label for="galY">Galaxy Y:</label></td>
        <td><input type="number" name="galY" id="galY"><br /></td>
        </tr>
        <tr>
        <td><label for="galRadius">+/- How Far?</label></td>
        <td><input type="number" name="galRadius" id="galRadius"><br /></td>
        </tr>
        <tr>
        <td><label for="locX">Local X:</label></td>
        <td><input type="number" name="locX" id="locX"><br /></td>
        </tr>
        <tr>
        <td><label for="locY">Local Y:</label></td>
        <td><input type="number" name="locY" id="locY"><br /></td>
        </tr>
        <tr>
        <td><label for="locRadius">+/- How Far?</label></td>
        <td><input type="number" name="locRadius" id="locRadius"><br /></td>
        </tr>
        <tr>
        <td colspan="2"><input type="submit" value="Search by Location"><br /></td>
        </table>
        </fieldset>
    </form>
            </div>
<?php } ?>
</body>
</html>